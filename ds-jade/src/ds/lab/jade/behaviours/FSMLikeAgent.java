package ds.lab.jade.behaviours;

/**
 * ***************************************************************
 * JADE - Java Agent DEvelopment Framework is a framework to develop
 * multi-agent systems in compliance with the FIPA specifications.
 * Copyright (C) 2000 CSELT S.p.A.
 *
 * GNU Lesser General Public License
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation,
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 * **************************************************************
 */

import jade.core.Agent;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;

/**
 * Adapted from Giovanni Caire's FSMAgent in examples.behaviours within JADE
 * distribution. A FSM-like agent showing how to specify FSM states and
 * transitions.
 *
 * @author s.mariani@unibo.it
 */
public class FSMLikeAgent extends Agent {

    /*
     * Just to experiment non-deterministic transitions.
     */
    private class RandomBheaviour extends StateBheaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        private int exit;
        private final int max;

        public RandomBheaviour(final String s, final int i) {
            super(s);
            this.max = i;
        }

        @Override
        public void action() {
            FSMLikeAgent.this.log("Inside state " + this.state);
            this.exit = (int) Math.floor(Math.random() * this.max);
            FSMLikeAgent.this.log("Exit value randomly drawn is " + this.exit);
        }

        /*
         * Needed to drive transitions.
         */
        @Override
        public int onEnd() {
            return this.exit;
        }

    }

    /*
     * Just prints the state we're in.
     */
    private class StateBheaviour extends OneShotBehaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        protected String state;

        public StateBheaviour(final String s) {
            this.state = s;
        }

        @Override
        public void action() {
            FSMLikeAgent.this.log("Inside state " + this.state);
        }

    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /*
     * State names.
     */
    private static final String STATE_A = "A";
    private static final String STATE_B = "B";
    private static final String STATE_C = "C";
    private static final String STATE_D = "D";

    private static final String STATE_E = "E";

    private static final String STATE_F = "F";

    private void log(final String msg) {
        System.out.println("[" + this.getLocalName() + "]: " + msg);
    }

    @Override
    protected void setup() {
        this.log("Started.");
        final FSMBehaviour fsm = new FSMBehaviour(this) {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public int onEnd() {
                FSMLikeAgent.this.log("Terminated.");
                this.myAgent.doDelete();
                return super.onEnd();
            }
        };
        /*
         * Need to tell JADE FSM starting state.
         */
        fsm.registerFirstState(new OneShotBehaviour(this) {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public void action() {
                FSMLikeAgent.this.log("Inside state A");
            }
        }, FSMLikeAgent.STATE_A);
        /*
         * Need to register "middle" states.
         */
        fsm.registerState(new StateBheaviour(FSMLikeAgent.STATE_B),
                FSMLikeAgent.STATE_B);
        fsm.registerState(new RandomBheaviour(FSMLikeAgent.STATE_C, 3),
                FSMLikeAgent.STATE_C);
        fsm.registerState(new StateBheaviour(FSMLikeAgent.STATE_D),
                FSMLikeAgent.STATE_D);
        fsm.registerState(new RandomBheaviour(FSMLikeAgent.STATE_E, 4),
                FSMLikeAgent.STATE_E);
        /*
         * Need to tell JADE FSM ending state.
         */
        fsm.registerLastState(new StateBheaviour(FSMLikeAgent.STATE_F),
                FSMLikeAgent.STATE_F);
        /*
         * Now let's register fsm transitions. 'Default' is used to register
         * transition to use when no exit values are specified.
         */
        fsm.registerDefaultTransition(FSMLikeAgent.STATE_A,
                FSMLikeAgent.STATE_B);
        fsm.registerDefaultTransition(FSMLikeAgent.STATE_B,
                FSMLikeAgent.STATE_C);
        fsm.registerTransition(FSMLikeAgent.STATE_C, FSMLikeAgent.STATE_C, 0);
        fsm.registerTransition(FSMLikeAgent.STATE_C, FSMLikeAgent.STATE_D, 1);
        fsm.registerTransition(FSMLikeAgent.STATE_C, FSMLikeAgent.STATE_A, 2);
        fsm.registerDefaultTransition(FSMLikeAgent.STATE_D,
                FSMLikeAgent.STATE_E);
        /*
         * If exit value is not 3, state E goes always to state B.
         */
        fsm.registerTransition(FSMLikeAgent.STATE_E, FSMLikeAgent.STATE_F, 3);
        fsm.registerDefaultTransition(FSMLikeAgent.STATE_E,
                FSMLikeAgent.STATE_B);
        /*
         * Finally add the behaviour.
         */
        this.addBehaviour(fsm);
    }

}
