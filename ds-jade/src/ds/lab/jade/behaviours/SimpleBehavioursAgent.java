package ds.lab.jade.behaviours;

/**
 * ***************************************************************
 * JADE - Java Agent DEvelopment Framework is a framework to develop
 * multi-agent systems in compliance with the FIPA specifications.
 * Copyright (C) 2000 CSELT S.p.A.
 *
 * GNU Lesser General Public License
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation,
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 * **************************************************************
 */

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;

/**
 * Adapted from Giovanni Caire's SimpleAgent in examples.behaviours within JADE
 * distribution. A simple agent showing concurrency in behaviours scheduling.
 *
 * @author s.mariani@unibo.it
 */
public class SimpleBehavioursAgent extends Agent {

    /*
     * Custom behaviour, not a composite one so extending <Behaviour> (since
     * extending <SimpleBehaviour> is not allowed by JADE).
     */
    private class FourStepsBehaviour extends Behaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        /*
         * Instance variable to keep track of computation state!
         */
        private int step;

        /*
         * State variable <step> ensures the correct computation is executed at
         * each scheduling round.
         */
        @Override
        public void action() {
            switch (this.step) {
                case 1:
                    SimpleBehavioursAgent.this.log("Doing operation 1...");
                    break;
                case 2:
                    SimpleBehavioursAgent.this
                            .log("Adding One-Shot Behaviour...");
                    /*
                     * Variable <myAgent> is provided by the JADE platform to
                     * refer to the agent to whom this behaviour was added.
                     */
                    SimpleBehavioursAgent.this
                            .addBehaviour(new OneShotBehaviour(this.myAgent) {

                                /**
                         *
                         */
                                private static final long serialVersionUID = 1L;

                                /*
                                 * For a One-Shot Behaviour, method <done()> is
                                 * already defined to always return <true>.
                                 */
                                @Override
                                public void action() {
                                    SimpleBehavioursAgent.this.log("One-Shot!");
                                }
                            });
                    break;
                case 3:
                    SimpleBehavioursAgent.this.log("Doing operation 3...");
                    break;
                case 4:
                    SimpleBehavioursAgent.this.log("Doing operation 4...");
                    break;
                default:
                    SimpleBehavioursAgent.this.log("Unknown operation!");
                    break;
            }
            this.step++;
        }

        /*
         * We decide our own termination condition!
         */
        @Override
        public boolean done() {
            return this.step > 4;
        }

        /*
         * By commenting this, you should see our agent performing the Cycling
         * Behaviour forever, because no one calls method <doDelete()>.
         */
        @Override
        public int onEnd() {
            SimpleBehavioursAgent.this.log("Terminated.");
//            this.myAgent.doDelete();
            /*
             * We don't care about termination values now, so leave them to
             * JADE...
             */
            return super.onEnd();
        }

        /*
         * We can override so-called "hook" (callback) methods called by the
         * JADE platform to manage behaviour objects lifecycle.
         */
        @Override
        public void onStart() {
            this.step = 1;
        }

    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /*
     * Still visible from (anonymous) inner classes!
     */
    private void log(final String msg) {
        System.out.println("[" + this.getName() + "]: " + msg);
    }

    /*
     * Here we add all the behaviours we wish to be ready to be scheduled as
     * soon as the agent is started.
     */
    @Override
    protected void setup() {
        this.log("Started.");
        /*
         * Anonymous inner class to define a trivial <CyclicBehaviour>. NB:
         * <this> identifies the enclosing type, hence our agent!
         */
        this.addBehaviour(new CyclicBehaviour(this) {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            /*
             * For a Cyclic Behaviour, method <done()> is already defined to
             * always return <false>.
             */
            @Override
            public void action() {
                SimpleBehavioursAgent.this.log("I'm cycling...");
            }
        });
        this.addBehaviour(new FourStepsBehaviour());
    }

}
