package ds.lab.jade.bookTrading.gui;

import jade.gui.GuiEvent;

/**
 * Interface for a safer implementation of JADE Agents - Java Swing GUIs
 * interactions.
 *
 * @author s.mariani@unibo.it
 */
public interface IBookBuyer {

    /*
     * Used by the GUI in the constructor.
     */
    String getAgentName();

    /*
     * Already present in JADE GuiAgent class, used by the GUI.
     */
    void postGuiEvent(GuiEvent e);

}
