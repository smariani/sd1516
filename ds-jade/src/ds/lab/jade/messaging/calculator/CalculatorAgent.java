package ds.lab.jade.messaging.calculator;

import jade.core.Agent;

/**
 * Simple Calculator agent executing one behaviour for each basic arithmetic
 * operator.
 *
 * @author s.mariani@unibo.it
 */
public class CalculatorAgent extends Agent {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private void log(final String msg) {
        System.out.println("[" + this.getName() + "]: " + msg);
    }

    @Override
    protected void setup() {
        this.log("Started.");
        this.addBehaviour(MyBehavioursFactory.createSumBehaviour());
        this.addBehaviour(MyBehavioursFactory.createSubBehaviour());
        this.addBehaviour(MyBehavioursFactory.createMulBehaviour());
        this.addBehaviour(MyBehavioursFactory.createDivBehaviour());
    }

}
