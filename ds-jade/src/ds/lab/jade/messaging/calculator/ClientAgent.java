package ds.lab.jade.messaging.calculator;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Client agent asking the Calculator Agent some random calculus to print out
 * the received results. NB: two distinct behaviours to concurrently manage
 * sending and receiving.
 *
 * @author s.mariani@unibo.it
 */
public class ClientAgent extends Agent {

    /*
     * One behaviour to receive replies.
     */
    private class MyReceiverBehaviour extends CyclicBehaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        private final MessageTemplate replyTemplate = MessageTemplate.or(
                MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                MessageTemplate.MatchPerformative(ACLMessage.NOT_UNDERSTOOD));

        @Override
        public void action() {
            final ACLMessage msg = this.myAgent.receive(this.replyTemplate);
            if (msg != null) {
                ClientAgent.this.log("Received message '"
                        + msg.getConversationId() + "' from agent <"
                        + msg.getSender().getName() + ">.");
                if (msg.getPerformative() == ACLMessage.NOT_UNDERSTOOD) {
                    ClientAgent.this.log("Made a mistake, shame on me!");
                } else if (msg.getPerformative() == ACLMessage.INFORM) {
                    Float res = null;
                    try {
                        res = Float.parseFloat(msg.getContent());
                    } catch (final NumberFormatException e) {
                        ClientAgent.this
                                .log("Unreadable content, shame on you!");
                    }
                    ClientAgent.this.log("Result is: " + res);
                }
            } else {
                this.block();
            }
        }
    }

    /*
     * One behaviour to send requests.
     */
    private class MySenderBehaviour extends TickerBehaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        private int id;

        public MySenderBehaviour(final Agent a, final long period) {
            super(a, period);
            this.id = 0;
        }

        @Override
        protected void onTick() {
            /*
             * Get 5 random values between 0 and 9.
             */
            final Float[] values = Util.genRandomValues(10, 5);
            final String content = Util.formatValues(values);
            ClientAgent.this.log("Prepared content: " + content);
            final ACLMessage req = new ACLMessage(ACLMessage.REQUEST);
            /*
             * We rely on the fact that the calculator is alive and is called
             * 'calculator'.
             */
            req.addReceiver(new AID("calculator", AID.ISLOCALNAME));
            req.setOntology(Util.drawnRandomOntology());
            ClientAgent.this.log("Chosen ontology: " + req.getOntology());
            req.setConversationId(this.myAgent.getLocalName() + "-" + this.id);
            req.setContent(content);
            ClientAgent.this.log("Sending message '" + req.getConversationId()
                    + "'...");
            ClientAgent.this.send(req);
            this.id++;
        }

    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private void log(final String msg) {
        System.out.println("[" + this.getName() + "]: " + msg);
    }

    @Override
    protected void setup() {
        this.log("Started.");
        this.addBehaviour(new MySenderBehaviour(this, 5000));
        this.addBehaviour(new MyReceiverBehaviour());
    }

}
