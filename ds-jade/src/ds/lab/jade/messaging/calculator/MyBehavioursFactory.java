package ds.lab.jade.messaging.calculator;

import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.LinkedList;

/**
 * Factory class to create "arithmetic" behaviours.
 *
 * @author s.mariani@unibo.it
 */
public final class MyBehavioursFactory {

    private MyBehavioursFactory() {
        /*
         * To avoid instantiation
         */
    }

    /**
     * The common-base Cyclic Behaviour behind the other four behaviours.
     */
    public class ArithmeticBehaviour extends CyclicBehaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        /*
         * The ontology used by the behaviour.
         */
        private final String ontology;
        /*
         * The template to use while receiving.
         */
        private final MessageTemplate template;

        public ArithmeticBehaviour(final MessageTemplate t, final String o) {
            this.template = t;
            this.ontology = o;
        }

        @Override
        public void action() {
            /*
             * Selective receive, different for each of the four behaviours.
             */
            final ACLMessage msg = this.myAgent.receive(this.template);
            if (msg != null) {
                this.log("Received message '" + msg.getConversationId()
                        + "' from agent <" + msg.getSender().getName() + ">.");
                this.log("Parsing content...");
                LinkedList<Float> values = new LinkedList<Float>();
                try {
                    /*
                     * Content should have a precise syntax.
                     */
                    values = Util.parse(msg.getContent());
                } catch (final NumberFormatException e) {
                    this.log("Unreadable content, informing sender...");
                    final ACLMessage reply = msg.createReply();
                    reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
                    reply.setContent("unreadable content");
                    this.myAgent.send(reply);
                }
                this.log("Content is: " + msg.getContent());
                this.log("Computing " + this.ontology + "...");
                /*
                 * Result should be computed based on 'ontology'.
                 */
                final Float result = this.computeResult(values);
                this.log("Result is: " + result);
                final ACLMessage reply = msg.createReply();
                reply.setPerformative(ACLMessage.INFORM);
                reply.setContent(result.toString());
                this.log("Sending result...");
                this.myAgent.send(reply);
            } else {
                this.log("Waiting for messages...");
                this.block();
            }
        }

        /*
         * Result must be computed differently based on the ontology passed to
         * constructor.
         */
        private Float computeResult(final LinkedList<Float> vs) {
            if ("summation".equals(this.ontology)) {
                Float result = 0.0f;
                while (!vs.isEmpty()) {
                    result += vs.removeFirst();
                }
                return result;
            } else if ("subtraction".equals(this.ontology)) {
                Float result = 0.0f;
                while (!vs.isEmpty()) {
                    result -= vs.removeFirst();
                }
                return result;
            } else if ("multiplication".equals(this.ontology)) {
                Float result = 1.0f;
                while (!vs.isEmpty()) {
                    result *= vs.removeFirst();
                }
                return result;
            } else {
                Float result = 1.0f;
                while (!vs.isEmpty()) {
                    result /= vs.removeFirst();
                }
                return result;
            }
        }

        private void log(final String msg) {
            System.out.println("  [" + this.getBehaviourName() + "]: " + msg);
        }

    }

    /**
     * DivBehaviour cares solely of 'division' messages.
     */
    public class DivBehaviour extends ArithmeticBehaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public DivBehaviour() {
            super(MessageTemplate.and(
                    MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                    MessageTemplate.MatchOntology("division")), "division");
        }
    }

    /**
     * MulBehaviour cares solely of 'multiplication' messages.
     */
    public class MulBehaviour extends ArithmeticBehaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public MulBehaviour() {
            super(MessageTemplate.and(
                    MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                    MessageTemplate.MatchOntology("multiplication")),
                    "multiplication");
        }
    }

    /**
     * SubBehaviour cares solely of 'subtraction' messages.
     */
    public class SubBehaviour extends ArithmeticBehaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public SubBehaviour() {
            super(MessageTemplate.and(
                    MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                    MessageTemplate.MatchOntology("subtraction")),
                    "subtraction");
        }
    }

    /**
     * SumBehaviour cares solely of 'summation' messages.
     */
    public class SumBehaviour extends ArithmeticBehaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public SumBehaviour() {
            super(MessageTemplate.and(
                    MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                    MessageTemplate.MatchOntology("summation")), "summation");
        }
    }

    public static DivBehaviour createDivBehaviour() {
        return new MyBehavioursFactory().new DivBehaviour();
    }

    public static MulBehaviour createMulBehaviour() {
        return new MyBehavioursFactory().new MulBehaviour();
    }

    public static SubBehaviour createSubBehaviour() {
        return new MyBehavioursFactory().new SubBehaviour();
    }

    /*
     * Factory methods to get the four different behaviours.
     */
    public static SumBehaviour createSumBehaviour() {
        return new MyBehavioursFactory().new SumBehaviour();
    }

}
